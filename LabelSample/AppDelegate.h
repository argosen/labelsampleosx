//
//  AppDelegate.h
//  ProgressBarHUDSample
//
//  Created by Ipad001 V5 on 19/05/14.
//  Copyright (c) 2014 --. All rights reserved.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>

@property (assign) IBOutlet NSWindow *window;

@end
