//
//  CustomView.m
//  ProgressBarHUDSample
//
//  Created by Ipad001 V5 on 19/05/14.
//  Copyright (c) 2014 --. All rights reserved.
//

#import "CustomView.h"

@implementation CustomView

- (id)initWithFrame:(NSRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code here.
    }
    return self;
}

- (void)drawRect:(NSRect)dirtyRect
{
    [super drawRect:dirtyRect];
    
    // Drawing code here.
}



//**************************//

-(IBAction)addText:(id)sender
{
    NSString *str = [outlet_textfield stringValue];
    NSLog(@"The captured string is: %@", str);
    
    // Without configuration
    NSSize s = [str sizeWithAttributes:@{NSFontAttributeName:outlet_label.font}];
    [outlet_label setFrameSize:s];
    [outlet_label setStringValue:str];
    NSLog(@"S size: %f", s.width);
    
    // With custom configuration
    [outlet_textfield2 setBezeled:NO];
    [outlet_textfield2 setDrawsBackground:NO];
    [outlet_textfield2 setEditable:NO];
    [outlet_textfield2 setSelectable:NO];
    
	outlet_textfield2.alignment = NSCenterTextAlignment;
	outlet_textfield2.backgroundColor = [NSColor clearColor];
	outlet_textfield2.textColor = [NSColor whiteColor];
	outlet_textfield2.font = [NSFont boldSystemFontOfSize:16.f];
    
    NSSize s2 = [str sizeWithAttributes:@{NSFontAttributeName:outlet_textfield2.font}];
    NSLog(@"S2 size: %f", s2.width);
    
    [outlet_textfield2 setFrameSize:s2];
    [outlet_textfield2 setStringValue:str];
    
    
    // Generated programmatically
    NSTextField *label = [[NSTextField alloc] initWithFrame:self.bounds];
    [label setBezeled:NO];
    [label setDrawsBackground:NO];
    [label setEditable:NO];
    [label setSelectable:NO];
	label.alignment = NSCenterTextAlignment;
	label.backgroundColor = [NSColor clearColor];
	label.textColor = [NSColor redColor];
	label.font = [NSFont boldSystemFontOfSize:16.f];
	[label setStringValue:str];
	[self addSubview:label];
    
    NSSize s3 = [str sizeWithAttributes:@{NSFontAttributeName:label.font}];
    NSLog(@"S3 size: %f", s3.width);
    [label setFrameSize:s3];
    //[label setFrameSize:NSMakeSize(145.f, 20.f)];
    //[self display];
}

//**************************//
@end
